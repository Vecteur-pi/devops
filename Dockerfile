# Utilisez une image Apache avec PHP intégré
FROM php:7.4-apache

# Mise à jour des sources de paquets
RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y apt-transport-https

# Ajout des sources de paquets pour PHP
RUN apt-get update && apt-get install -y gnupg && \
    echo "deb https://packages.sury.org/php/ bullseye main" > /etc/apt/sources.list.d/php.list && \
    curl -fsSL https://packages.sury.org/php/apt.gpg | gpg --dearmor -o /etc/apt/trusted.gpg.d/php.gpg && \
    apt-get purge -y gnupg && apt-get autoremove -y

# Mise à jour des sources de paquets à nouveau
RUN apt-get update

# Installation des dépendances nécessaires
RUN apt-get install -y libxml2-dev libzip-dev libpng-dev libjpeg-dev libfreetype6-dev libjpeg62-turbo-dev libicu-dev libcurl4-openssl-dev libssl-dev libc-client-dev libkrb5-dev libonig-dev

# Configuration des extensions
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-configure imap --with-imap --with-imap-ssl --with-kerberos

# Installation des extensions PHP nécessaires
RUN docker-php-ext-install mysqli pdo_mysql xml zip mbstring gd intl curl imap calendar

# Téléchargez et installez Dolibarr
ADD https://github.com/Dolibarr/dolibarr/archive/refs/tags/15.0.3.tar.gz /var/www/html
RUN tar -zxvf /var/www/html/15.0.3.tar.gz -C /var/www/html/ && \
    mv /var/www/html/dolibarr-15.0.3/htdocs /var/www/html/dolibarr && \
    rm -rf /var/www/html/dolibarr-15.0.3

# Configurez Apache pour Dolibarr
COPY ./dolibarr.conf /etc/apache2/sites-available/
RUN a2dissite 000-default && a2ensite dolibarr && a2enmod rewrite

# Créez le répertoire de documents et configurez les permissions
RUN mkdir -p /var/www/html/dolibarr/documents && \
    chown -R www-data:www-data /var/www/html/dolibarr/documents

# Créez le répertoire de documents et configurez les permissions
RUN mkdir -p /var/www/html/dolibarr/documents && \
    mkdir -p /var/www/html/dolibarr/htdocs/conf && \
    touch /var/www/html/dolibarr/htdocs/conf/conf.php && \
    chmod 666 /var/www/html/dolibarr/htdocs/conf/conf.php && \  
    chown -R www-data:www-data /var/www/html/dolibarr

# Exposez le port 80
EXPOSE 80

# Démarrez Apache au démarrage du conteneur
CMD ["apache2-foreground"]
